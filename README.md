# reunifica
## Descripción:
reunifica lineas de texto en párrafos; utilizable para textos extraídos de archivos pdf

## Sintaxis:
Ejecutar
`runhaskell reunifica.hs nombre_archivo.txt`
y genera `nombre_archivo_nuevo.txt` con el texto formateado resultante.