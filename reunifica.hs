{-
    reunifica lineas de texto en párrafos; utilizable para textos extraídos de archivos pdf
    autor: marcarand
    sitio: https://marcarand.codeberg.page/
    GNU GPLv3
-}

import System.Environment

main :: IO ()
main = do
    [arg] <- getArgs
    lineas <- lista_lineas arg
    let unificado = reunir lineas $ ancho lineas
    let nuevo_nombre = archivo_salida arg
    writeFile nuevo_nombre unificado


lista_lineas :: FilePath -> IO [String]
lista_lineas archivo = do
    texto <- readFile archivo
    let lineas = lines texto
    return lineas

ancho :: [String] -> Int
ancho lista
    | lista == [] = 0
    | otherwise   = maximum (map length lista)

esVacia :: String -> Bool
esVacia linea
    | linea == "\n" = True
    | otherwise     = False

esTrunca :: String -> Bool
esTrunca linea
    | fin == '-' || fin == '\x2013' = True
    | otherwise                     = False
    where fin = final linea

-- | La función establece si es punto aparte, con algunas excepciones:
-- por ejemplo, si termina con signo de admiración o de interrogación.
-- Seguramente, si hiciera falta, se podría incluir.
esAparte :: String -> String -> Int -> Bool
esAparte linea siguiente margen
    | fin == '.' && excede = True
    | otherwise            = False
    where
        excede = margen - (length linea) > length (inicial (words siguiente))
        fin = final linea

inicial :: [String] -> String
inicial [] = ""
inicial (s:_) = s

final :: [Char] -> Char
final []  = '\n'
final [x] = x
final (_:xs) = final xs

-- | Devuelve la cadena excepto el último carácter.
truncar :: String -> String
truncar []     = ""
truncar [s]    = ""
truncar (s:ss) = s : truncar ss

reunir :: [String] -> Int -> String
reunir [] _  = ""
reunir [s] _ = s
reunir (s:ss) margen
    | esVacia  s                     = "\n" ++ reunir ss margen
    | esTrunca s                     = truncar s ++ reunir ss margen
    | esAparte s (inicial ss) margen = s ++ "\n" ++ reunir ss margen
    | otherwise                      = s ++ " "  ++ reunir ss margen

archivo_salida :: String -> String
archivo_salida nom = reverse (tail nombre) ++ "_nuevo" ++ [head nombre] ++ reverse extension
    where (extension, nombre) = span (/='.') $ reverse nom

